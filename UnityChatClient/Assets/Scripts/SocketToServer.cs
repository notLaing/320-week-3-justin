﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using TMPro;
using UnityEngine;

public class SocketToServer : MonoBehaviour
{
    public string Host = "127.0.0.1";
    public ushort Port = 8080; //0-65,535
    public TextMeshProUGUI ChatDisplay;
    public TMP_InputField InputDisplay;


    private TcpClient socket = new TcpClient();

    // Start is called before the first frame update
    void Start()
    {
        InputDisplay.interactable = false;
        InputDisplay.onEndEdit.AddListener(OnInputFieldEdit);
        ConnectToServer();
    }

    private async void ConnectToServer()
    {
        try
        {
            await socket.ConnectAsync(Host, Port);
            AddMessageToChatDisplay("<color=#005500>Successfully connected to server</color>");
            InputDisplay.interactable = true;
        }
        catch (Exception e)
        {
            AddMessageToChatDisplay("<color=#550000>Error: " + e.Message + "</color>");
            return;
        }

        /*while (true)
        {
            byte[] data = new byte[socket.Available];
            await socket.GetStream().ReadAsync(data, 0, data.Length);
            if (data.Length > 0)
            {
                string str = Encoding.ASCII.GetString(data);
                Debug.Log(str);
                int ind = str.IndexOf("CHAT");
                //contains "CHAT" and is a full packet
                if(ind > 0 && str.Substring(ind).IndexOf('\n') > 0)
                {
                    string[] substr = str.Substring(ind).Split('\t');
                    AddMessageToChatDisplay(substr[1]);
                }
            }
               
        }*/
        while (true)
        {
            byte[] data = new byte[socket.Available];
            await socket.GetStream().ReadAsync(data, 0, data.Length);
            if (data.Length > 0)
            {
                //Debug.Log(Encoding.ASCII.GetString(data));
                ////AddMessageToChatDisplay(Encoding.ASCII.GetString(data));
                string[] content = Encoding.ASCII.GetString(data).Split('\t');
                switch(content[0])
                {
                    case "CHAT":
                        AddMessageToChatDisplay("<color=#0000FF>CHAT\t" + content[1] + "\t" + content[2] + "</color>");
                        break;
                    case "ANNC":
                        //no functionality yet
                        break;
                    case "DMSG":
                        AddMessageToChatDisplay("<color=#DD33DD>DMSG\t" + content[1] + " whispered:\t" + content[2] + "</color>");
                        break;
                    case "NOKY\nLIST":
                        string newList = "<color=#55FF55>NOKY\nLIST";
                        for (int i = 1; i < content.Length; ++i)
                        {
                            newList += "\t" + content[i];
                        }
                        AddMessageToChatDisplay(newList + "</color>");
                        break;
                    case "NBAD":
                        AddMessageToChatDisplay("<color=#AA2222>NBAD\t" + content[1] + "</color>");
                        break;
                    case "LIST":
                        string list = "<color=#55FF55>LIST";
                        for(int i = 1; i < content.Length; ++ i)
                        {
                            list += "\t" + content[i];
                        }
                        AddMessageToChatDisplay(list + "</color>");
                        break;
                }
            }
        }

    }

    public void AddMessageToChatDisplay(string text)
    {
        ChatDisplay.text += text + "\n";
    }

    public void OnInputFieldEdit(string s)
    {
        SendMessageToServer(s);
        InputDisplay.text = "";
        InputDisplay.Select();
        InputDisplay.ActivateInputField();
    }

    public void SendMessageToServer(string message)
    {
        //TODO: create packets
        //using League of Legends command style: /cmd
        if(message[0] == '/')
        {
            message = message.Substring(1, message.Length - 1);
            string[] cmd = message.Split(' ');

            switch(cmd[0].ToLower())
            {
                case "msg":
                    message = "DMSG\t" + cmd[1] + "\t" + cmd[2] + "\n";
                    break;
                case "name":
                    message = "NAME\t" + cmd[1] + "\n";
                    break;
                case "list":
                    message = "LIST\n";
                    break;
            }
        }
        else
        {
            //normal chat packet
            message = "CHAT\t" + message + "\n";
        }
        byte[] data = Encoding.ASCII.GetBytes(message);
        socket.GetStream().Write(data, 0, data.Length);
    }

}
