const net = require("net");

//represents our protocol
//packets FROM the server TO the client
const Packet = {
    charEndOfPacket: '\n',
    charDelimeter: '\t',
    buildChat: function (usernameFrom, message) {
        return this.buildFromParts(["CHAT", usernameFrom, message]);
    },
    buildAnnouncement: function (message) {
        return this.buildFromParts(["ANNC", message]);
    },
    buildNameOkay: function () {
        return this.buildFromParts(["NOKY"]);
    },
    buildNameBad: function (error) {
        return this.buildFromParts(["NBAD", error]);
    },
    buildDM: function (sender, message) {
        return this.buildFromParts(["DMSG", sender, message]);
    },
    buildList: function (arrayOfClients) {
        const arrayOfUsernames = [];

        arrayOfClients.forEach(c => {
            if (c.username) arrayOfUsernames.push(c.username);
            else arrayOfUsernames.push(c.socket.localAddress);
        });

        arrayOfUsernames.unshift("LIST");

        return this.buildFromParts(arrayOfUsernames);
    },
    buildFromParts: function (arr) {
        return arr.join(this.charDelimeter) + this.charEndOfPacket;
    }


}

class Client {
    constructor(socket, server) {
        this.buffer = "";
        this.username = "";
        this.socket = socket;
        this.server = server;
        this.socket.on("error", (e) => this.onError(e));
        this.socket.on("close", (b) => this.onDisconnect(b));
        this.socket.on("data", (d) => this.onData(d));
    }

    onError(err) {
        console.log("ERROR with " + this.socket.localAddress + " : " + err);
    }

    onDisconnect(hadError) {
        this.server.onClientDisconnect(this);
    }

    onData(data) {
        this.buffer += data;



        //example user input: CHAT\t(message)\nNAME\t(name)\nLIST\nCHAT\t(user

        //split our buffer apart into array of "packets" (commands/functions)
        const packets = this.buffer.split("\n");

        this.buffer = packets.pop();


        console.log(packets.length + " new packets received from " + this.socket.localAddress);

        //handle all COMPLETE packets
        packets.forEach(p => this.handlePacket(p));
    }

    handlePacket(packet) {
        //split our packet into parts
        const parts = packet.split('\t');
        
        //packets from client to server
        switch (parts[0]) {
            case "CHAT":
                server.broadcast(Packet.buildChat(this.username, parts[1]));
                break;
            case "DMSG":
                //TODO: make it work
                server.dm(Packet.buildDM(this.username, parts[2]), parts[1]);
                break;
            case "NAME":
                const newName = parts[1];

                //TODO: accept or reject new name
                if(newName == "Crap")
                {
                    this.sendPacket(Packet.buildNameBad("No bad words"));
                    break;
                }
                
                let allNames = server.clients;
                let taken = false;
                allNames.forEach(c => {
                    if(c.username == newName)
                    {
                        taken = true;
                        //break;
                    }
                });

                //below is accepting it
                if(!taken)
                {
                    this.username = newName;
                    this.sendPacket(Packet.buildNameOkay());
                }
                else
                {
                    this.sendPacket(Packet.buildNameBad("Username is taken"));
                }
                

                //TODO: send LIST packet to all users
                server.broadcast(Packet.buildList(this.server.clients));
                break;
            case "LIST":
                this.sendPacket(Packet.buildList(this.server.clients));
                break;
        }

    }

    //packets from server TO client
    sendPacket(packet) {
        this.socket.write(packet);
    }

}

class Server {

    constructor() {
        this.port = 8080;
        this.clients = [];

        this.socket = net.createServer({}, c => this.onClientConnect(c));
        this.socket.on("error", e => this.onError(e));
        this.socket.listen(
            { port: this.port },
            () => this.onStartListen());
    }

    onError(errMsg) {
        console.log("ERROR: " + errMsg);
    }

    onClientConnect(socketToClient) {

        console.log("A new client connected from " + socketToClient.localAddress);
        const client = new Client(socketToClient, this);

        this.clients.push(client);
        //TODO: broadcast a LIST packet to everyone
        this.broadcast(Packet.buildList(this.clients));
    }

    onClientDisconnect(client) {
        this.clients.splice(this.clients.indexOf(client), 1);
        //TODO: broadcast a LIST packet to everyone
        this.broadcast(Packet.buildList(this.clients));
    }

    onStartListen() {
        console.log("the server is now listening on port " + this.port);
    }

    broadcast(packet) {
        this.clients.forEach(c => {
            c.sendPacket(packet);
        })
    }

    dm(packet, target) {
        //redundant form and split, but I wanted to try using it again
        const dmParts = packet.split('\t');

        let found = false;
        this.clients.forEach(c => {
            if(c.username == target)
            {
                c.sendPacket("DMSG\t" + dmParts[1] + '\t' + dmParts[2] + '\n');
                //break;
            }
        });
    }

}

const server = new Server();